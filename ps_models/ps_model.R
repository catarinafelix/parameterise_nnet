setwd("C:/Users/Catarina/nn_shiny/")
load("dsCompleto.RData")
load("formulas.RData")

setwd("C:/Users/Catarina/nn_shiny/ps_models/")
attributes <- ds[,2:(ncol(ds)-3)]

targets <- ds[,-c(1:(ncol(ds)-3))] 
targets.names <- colnames(targets)

forms <- formulas$regression

#### neurons ####
data <- cbind(attributes, target=targets$neurons)
model <- lm(formula = forms$lmFuncs$neurons, data = data)
save(model,file="model_n.RData")

#### decay ####
data <- cbind(attributes, target=targets$decay)
model <- lm(formula = forms$lmFuncs$decay, data = data)
save(model,file="model_d.RData")

#### abstol ####
data <- cbind(attributes, target=targets$abstol)
model <- lm(formula = forms$lmFuncs$abstol, data = data)
save(model,file="model_a.RData")

attributes <- ds[,-1]
#### all neurons ####
data <- cbind(attributes, target=targets$neurons)
model <- lm(formula = paste0(forms$lmFuncs$neurons," + neurons + decay + abstol"), data = data)
save(model,file="model_n_all.RData")

#### all decay ####
data <- cbind(attributes, target=targets$decay)
model <- lm(formula = paste0(forms$lmFuncs$decay," + neurons + decay + abstol"), data = data)
save(model,file="model_d_all.RData")

#### all abstol ####
data <- cbind(attributes, target=targets$abstol)
model <- lm(formula = paste0(forms$lmFuncs$abstol," + neurons + decay + abstol"), data = data)
save(model,file="model_a_all.RData")
