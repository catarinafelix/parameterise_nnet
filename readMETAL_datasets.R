# dataset.names <- c("ConcreteData.csv_Concrete.compressive.strength.MPa..megapascals.",
#                    "winequality-red.csv_quality",
#                    "winequality-white.csv_quality",
#                    "o-ring-erosion-only.data_Number.experiencing.thermal.distress",
#                    "o-ring-erosion-or-blowby.data_Number.experiencing.thermal.distress",
#                    "slumpTest.data_SLUMP.cm.",
#                    "slumpTest.data_FLOW.cm.",
#                    "slumpTest.data_Compressive.Strength..28.day..Mpa.",
#                    "parkinsonsUpdrs.data_motor_UPDRS",
#                    "parkinsonsUpdrs.data_total_UPDRS",
#                    "airfoilSelfNoise.dat_Scaled.sound.pressure.level",
#                    "ENB2012data.csv_Y1",
#                    "ENB2012data.csv_Y2",
#                    "yachtHydrodynamics.data_Residuary.resistance.per.unit.weight.of.displacement")



load("temp/dataset.names.RData")
datasets <- list(NULL)

for (i in 1:length(dataset.names)) {
	print(dataset.names[i])

#in wd you must name the working directory of your data
wd <- paste("./regression/",dataset.names[1], sep="")
setwd(wd)


# ReadAttrsInfo <- function(fileName,attrs.exts=c(".domain",".names")) {
# 	# Read .names file
# 	attrsFile <- FindFile(fileName,attrs.exts,"variables")
#    
# 	lines <- readLines(attrsFile)
# 
# 	# Identify names and types of attributes	        
# 	original.attr.name <- sapply(subset(lines,regexpr(":",lines)!=-1),
#                              FUN="ExtractAttrName",USE.NAMES= FALSE)
# 	return(c(original.attr.name,"class"))                           
# }
# 
# FindFile <- function(fileName,fileExts,fileType) {
# 	ext.ind <- match(TRUE,file.exists(paste(fileName,fileExts,sep="")))
# 
# 	if(is.na(ext.ind)) stop(paste(fileType,"file missing"))
#              
# 	paste(fileName,fileExts[ext.ind],sep="")
# }
# 
# ExtractAttrName <- function(line) substr(line,1,regexpr("([ \t]*:)+",line)-1)

coln <- ReadAttrsInfo(dataset.names[i],attrs.exts=".names")
datasets[[i]] <- read.table(paste(dataset.names[i],".data",sep=""), check.names = TRUE, col.names=coln$attributes$attr.name, sep=" ", strip.white=TRUE, na.strings="NA", row.names=NULL)
aux <- datasets[[i]]
aux[aux=="?"] <- NA
aux[aux=="NA"] <- NA
datasets[[i]] <- aux
}

names(datasets) <- dataset.names

setwd("../")
setwd("../")